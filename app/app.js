/**
Angular application template
author: Ivan Lausuch Sales
Started on 2014
*/

console.info("----------------- New execution -----------------");

//----------------------------------------------------------------------------------
// ANGULAR APPLICATION
//----------------------------------------------------------------------------------

//TODO : All all needed modules
//NOTE: In mobile apps is required ["ui.router"]
var app = angular.module('app', ["ionic"]);

app.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
    
    console.info("Ionic Ready");
    mc.start();
  });
})





//----------------------------------------------------------------------------------
// MAIN CONTROLER
//----------------------------------------------------------------------------------
//Create default main controller
var mc;

app.controller("MainController", function($rootScope,$scope,$timeout,$http,$q){

	//Set mc global variable
	mc=this;
	
	//Easy access to main controls
	this.$rootScope=$rootScope;
	this.$scope=$scope;
	this.$timeout=$timeout;
	this.$http=$http;
	this.$q=$q;
	this.c=undefined;
	this.route={};
	
	//Complete main controller with functions
	completeMainController();
	completeMobileMainController();
	
	this.onReady=function(){
		console.info("app/app.js - MainController - onReady - Implement here user check or other needs");
	}
	
	this.onPress=function(item,menu){
		console.info("app/app.js - MainController - onPress - Implement here buttons actions from menu or header");
	}
	
	//TODO : Define here all variables or functions you need
	
	//Force to start Main controller
	console.info("Maincontroller created");
});

		
//----------------------------------------------------------------------------------
// ROUTER CONTROLER
//----------------------------------------------------------------------------------

/**
This is route configuration. Is where you define all routes in your application
*/
app.config(function($stateProvider, $urlRouterProvider) {
    //Default route
    $urlRouterProvider.otherwise('/');
    
    //TODO : Define all routes ($stateProvider, route(string), controller(string) [, persistent(bool) ,prerequisitesChecker(func)])
    /*
	NOTE:PrerequisitesChecker must be a funcion with these params (callback,$stateParams,$location) 
	and mush call to callback when finish
	*/
    stateRouteAdd($stateProvider,"/","index"
	    ,{
	    	persistent:true,
	    	prerequisites:function(callback,$stateParams,$location){
		    	console.info("app/app.js - Route config - / - Prerequisites checker (remove if you don't need it)");
		    	callback();	
		    },
		    onFinish:function($stateParams){
			    console.info("app/app.js - Route config - / - onFinish");
		    }
    	}
    		
	);  
	
	stateRouteAdd($stateProvider,"/page2","page2");
	
	
});
